#!/bin/bash

# Written by Julien Van Loo
# 05/04/2014

# Regroupe tout les scripts relatifs à l'unif dans un menu, l'ID ULB peut être passé en paramètre

function vocalEcho {
	if [ "$2" != "no-text" ]; then
		echo "$1"
	fi
	if [[ $vocal == 1 ]]; then
		mpg321 "http://translate.google.com/translate_tts?tl=fr&q=$(echo $1 | sed 's/\ /%20/g')" -q
	fi
}

function connect {
	# $1 connect ou replicate; $2 paramètres SSH
	nbrMachines=30
	serveurDispo=0

	vocalEcho "Recherche d'un serveur disponible ..."

	for prefix in 'sca-xt' 'romeo' 'roxane' ; do
		for i in `seq 1 $nbrMachines` ; do
			if [ $i -lt 10 ] && [ $prefix == 'sca-xt' ]; then
				nomServeur="${prefix}0$i"
			else
				nomServeur="${prefix}$i"
			fi
			if [ -z $(nc -w 1 "$nomServeur.ulb.ac.be" 22 | cut -c -3) ]; then
				echo "Serveur $nomServeur HS"
			else
				vocalEcho "Serveur $nomServeur est disponible."
				serveurDispo=1
				break 2
			fi
		done
	done
	if [ $serveurDispo -eq 0 ]; then
		vocalEcho "Il n'y a malheureusement aucun serveur accessible."
	else
		if [ "$1" == "connect" ]; then
			`x-terminal-emulator -e "ssh $nom@$nomServeur.ulb.ac.be $2"`
		elif [ "$1" == "replicate" ]; then
			if [ ! -e "/home/$USER/Session_$nom" ]; then
				`mkdir ~/Session_$nom`
			fi
			vocalEcho "Début de la synchronisation ..."
			echo "`rsync -arvh --delete --backup --backup-dir=/home/$USER/Session_$nom/FichiersSupprimes $nom@$nomServeur.ulb.ac.be:/home/$nom/ /home/$USER/Session_$nom/CopieSession/`"
		elif [ "$1" == "copy" ]; then
			vocalEcho "Copie vers la session $nom"
			vocalEcho "Source locale" "no-text"
			read -p "Source locale : " src
			vocalEcho "Destination sur $nomServeur" "no-text"
			read -p "Destination sur $nomServeur : " dst
			if [ -d $src ]; then
				scp -r "$src" "$nom@$nomServeur.ulb.ac.be:$dst"
			else
				scp "$src" "$nom@$nomServeur.ulb.ac.be:$dst"
			fi
			vocalEcho "Copie OK"
		else
			vocalEcho "Mauvais argument 1 pour connect"
		fi
	fi
}

function scan {
	# Scan les PC de l'ULB et retourne une liste des PC allumés.
	# $1 commande ou vide

	nbrMachines=30
	nbrServeursDispo=0

	if [ -n "$1" ]; then
		vocalEcho "Mot de passe pour $nom" "no-text"	
		read -p "Mot de passe pour $nom : " -s password
		echo
	fi
	vocalEcho "Obtention de la liste des serveurs disponibles..."

	for prefix in 'sca-xt' 'romeo' 'roxane' ; do
		for i in `seq 1 $nbrMachines` ; do
			if [ $i -lt 10 ] && [ $prefix == 'sca-xt' ]; then
				nomServeur="${prefix}0$i"
			else
				nomServeur="${prefix}$i"
			fi
			if [ -z $(nc -w 1 "$nomServeur.ulb.ac.be" 22 | cut -c -3) ]; then
				echo -n
				# Le serveur est éteint ou inexistant
			else
				if [ -n "$1" ]; then
					echo -n "$nomServeur -> "
					expect -c "spawn -noecho ssh -q \"$nom@$nomServeur.ulb.ac.be\" \"$1\"
expect \"yes/no\" {
send \"yes\r\"
expect \"*?assword:\" {
send \"$password\r\" }
} \"*?assword:\" {
send \"$password\r\" }
interact"
				else
					echo "$nomServeur"
				fi
				let "nbrServeursDispo = nbrServeursDispo + 1"
			fi
		done
	done
	vocalEcho "$nbrServeursDispo serveurs disponibles."
}

function findUser {
		# Scan les PC de l'ULB et retourne une liste des PC allumés.
	# $1 commande ou vide

	nbrMachines=30

	vocalEcho "Mot de passe pour $nom" "no-text"
	read -p "Mot de passe pour $nom : " -s password
	echo
	vocalEcho "Recherche de $alterUser..."

	for prefix in 'sca-xt' 'romeo' 'roxane' ; do
		for i in `seq 1 $nbrMachines` ; do
			if [ $i -lt 10 ] && [ $prefix == 'sca-xt' ]; then
				nomServeur="${prefix}0$i"
			else
				nomServeur="${prefix}$i"
			fi
			if [ -z $(nc -w 1 "$nomServeur.ulb.ac.be" 22 | cut -c -3) ]; then
				echo -n
				# Le serveur est éteint ou inexistant
			else
				expectRes=$(expect -c "spawn -noecho ssh -q \"$nom@$nomServeur.ulb.ac.be\" \"w\"
expect \"yes/no\" {
send \"yes\r\"
expect \"*?assword:\" {
send \"$password\r\" }
} \"*?assword:\" {
send \"$password\r\" }
interact" | grep $alterUser)
				if [[ "$expectRes" == *tty8* ]]; then
					vocalEcho "$alterUser actif sans terminal ouvert sur $nomServeur, impossible de discuter"
					`x-terminal-emulator -e "ssh $nom@$nomServeur.ulb.ac.be"`
					break 2
				elif [[ "$expectRes" == *tty* ]] || [[ "$expectRes" == *pts* ]]; then
					vocalEcho "$alterUser disponible sur $nomServeur"
					`x-terminal-emulator -e "ssh $nom@$nomServeur.ulb.ac.be"`
					#expect -c "spawn -noecho ssh -q \"$nom@$nomServeur.ulb.ac.be\" \"write $chatName\"
#expect \"yes/no\" {
#send \"yes\r\"
#expect \"*?assword:\" {
#send \"$password\r\" }
#} \"*?assword:\" {
#send \"$password\r\" }
#interact"
					break 2
				fi
			fi
		done
	done
}

echo -e "***************ULB Tools***************\n* Written by Julien Van Loo           *\n* 08/04/2014                          *\n* Version 0.9 Beta                    *\n***************************************"
if [ -z $1 ]; then
	vocalEcho "Entrez votre ID ULB" "no-text"
	read -p 'Entrez votre ID ULB : ' nom
else
	nom=$1
fi
vocal=0
while [ "$choix" != "quit" ] && [ "$choix" != "exit" ] && [ "$choix" != "q" ]; do
	vocalEcho "Entrez une commande" "no-text"
	read -p '> ' choix
	
	case "$choix" in
		c | connect) connect connect
		;;
		gc | gconnect) connect connect -XY
		;;
		r | replicate) connect replicate
		;;
		cp | copy) connect copy
		;;
		s | scan) scan
		;;
		us | userscan) scan users
		;;
		mc | masscommand) vocalEcho "Commande a effectuer en masse" "no-text"
		read -p "Commande à effectuer en masse : " command
		scan $command
		;;
		find) vocalEcho "ID du correspondant" "no-text"
		read -p "ID du correspondant : " alterUser
		findUser
		;;
		u | user) vocalEcho "Entrez le nouvel ID ULB" "no-text"
		read -p 'Entrez le nouvel ID ULB : ' nom
		;;
		speak) vocal=1
		vocalEcho "Annonce vocale active"
		;;
		mute) vocal=0
		echo "Annonce vocale désactivée"
		;;
		h | help) echo -e "Commandes :\n- h           affiche l'aide\n  help\n- q           quitter\n  quit\n  exit\n- u           Changer d'utilisateur\n  user\n- speak       Activer l'annonce vocale (défaut=off)\n- mute        Désactiver annonce vocale\n- c           Se connecter à sa session ULB en mode console\n  connect\n- gc          Se connecter à sa session ULB en mode graphique\n  gconnect    (Serveur X nécessaire)\n- r           Faire une copie de sa session ULB en local\n  replicate\n- cp          Copier vers la session ULB (pas de ~)\n  copy\n- s           Scanner les ordinateurs allumées à l'ULB\n  scan\n- us          Scanner qui est connecté à un ordinateur ULB\n  userscan\n- mc          Effectuer une commande sur tout les ordinateurs ULB allumés\n  masscommand\n- find        Se connecter à la machine d'une personne donnée"
		;;
		q | quit | exit) vocalEcho "Fermeture"
		;;
		*) vocalEcho "Commande invalide, help pour connaitre la liste des commandes"
		;;
	esac
done
