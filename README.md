# README #

## Aperçu des fonctionalités ##

ulbTools permet de gérer sa session ULB à distance, on peut s'y connecter, copier des fichiers, en faire un backup. Le script offre un scan des machines de l'ULB et permet aussi d'exécuter une commande massivement sur toute les machines accessibles.

La commande *help* ou *h* liste les commandes disponibles.

## Installation de ulbTools ##

* Downloader le fichier ulbTools.sh de ce dépôt
* Rendre le fichier exécutable

```
#!sh

chmod +x ulbTools.sh
```
* Paquets requis pour le bon fonctionnement du script : rsync, openssh-client, netcat, expect et mpg321 (pour l'audio uniquement)
Par exemple pour Debian/Ubuntu

```
#!sh

sudo apt-get install rsync openssh-client netcat expect mpg321
```
* Lancer en étant dans le dossier contenant le fichier ulbTools.sh

```
#!sh

./ulbTools.sh
```

